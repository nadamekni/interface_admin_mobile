package com.example.interfaceadmin;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListUser_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListUser_Fragment extends Fragment {
    String [] nom={"mekni","snoussi","manai"};
    String [] prenom={"nada","marwen","donia"};
    String [] email={"nadamekni@gmail.com","marwensnoussi@gmail.com","doniamanai@gmail.com"};
    String [] dn={"19/11/2000","07/03/2000","07/07/2000"};
    String [] cin={"14278474","14253274","11223355"};
    ListView l;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ListUser_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListUser_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListUser_Fragment newInstance(String param1, String param2) {
        ListUser_Fragment fragment = new ListUser_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_list_user_, container, false);
        l=view.findViewById(R.id.l);
        PlayerListAdapter adapter=new PlayerListAdapter(getActivity(),cin,nom,prenom,email,dn);
        l.setAdapter(adapter);
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long position) {
                AlertDialog.Builder a=new AlertDialog.Builder(getActivity());
                a.setTitle("Client "+cin[(int) position]);
                a.setMessage("\n"+cin[(int) position]+"\n"+nom[(int) position]+"\n"+prenom[(int) position]+"\n"+email[(int) position]+"\n"+dn[(int) position]);
                a.setPositiveButton("OK",null);
                a.show();
            }
        });


        // Inflate the layout for this fragment
        return view;
    }
}