package com.example.interfaceadmin;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

public class PlayerListAdapter extends ArrayAdapter {
    Activity context;
    String[] nom;
    String[] prenom;
    String[] cin;
    String[] email;
    String[] dn;

    public PlayerListAdapter(Activity context,String[] cin, String[] nom, String[] prenom,String[] email ,String[] dn) {
        super(context, R.layout.element,nom);
        this.context = context;
        this.nom = nom;
        this.prenom = prenom;
        this.cin = cin;
        this.email = email;
        this.dn = dn;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater li = context.getLayoutInflater();
        View view = li.inflate(R.layout.element, null, true);
        TextView t1=view.findViewById(R.id.name);
        TextView t2=view.findViewById(R.id.prenom);
        TextView t3=view.findViewById(R.id.email);
        TextView t4=view.findViewById(R.id.cin);
        TextView t5=view.findViewById(R.id.dn);
        ImageView delete=view.findViewById(R.id.delete);

        t1.setText(nom[position]);
        t2.setText(prenom[position]);
        t3.setText(email[position]);
        t5.setText(dn[position]);
        t4.setText(cin[position]);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder a= new AlertDialog.Builder(context);
                a.setTitle("delete ");

                a.setPositiveButton("delete", null);
                a.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // finish();
                        Toast.makeText(context,"canceled",Toast.LENGTH_LONG).show();
                    }
                });
                a.show();
            }
        });

        return view;
    }

}
